# D&D Roller
This is a Rocket-based API for fetching the probabilities of specific dice rolls

## Building
Both `gcc` and a nightly build of Rust (currently 1.47) are required to build

Once the dependencies are installed: pull the project, navigate into the root directory, and run `cargo run`. That should be all that's required

## Usage
Right now the API is going to be changing a lot, so I'm not going to detail it yet. Currently, the API will be hosted on `localhost:8000/`
