use rusqlite::{params, Connection, Result, NO_PARAMS};

#[derive(Debug)]
pub struct Cat {
    pub id: i32,
    pub name: String,
    pub coat: String,
    pub weight: String,
}

impl PartialEq for Cat {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.name == other.name && self.coat == other.coat && self.weight == other.weight
    }
}

pub fn init() -> Result<()> {
    let conn = Connection::open("cats.db")?;

    conn.execute(
        "create table if not exists cat_colors (
             id integer primary key,
             name text not null,
             coat text not null,
             weight text 
         )",
        NO_PARAMS,
    )?;
    Ok(())
}

pub fn add_cat(cat: &Cat) -> Result<(), rusqlite::Error> {
    let conn = Connection::open("cats.db")?;
    conn.execute(
        "insert into cat_colors (
        name, coat, weight)
        values (?1, ?2, ?3);",
        params!(cat.name, cat.coat, cat.weight),
    )?;
    Ok(())
}

pub fn get_cats() -> Result<Vec<Cat>, rusqlite::Error> {
    let mut cats: Vec<Cat> = Vec::new();

    let conn = Connection::open("cats.db")?;

    let mut query = conn.prepare("SELECT * FROM cat_colors;")?;

    let results = query.query_map(NO_PARAMS, |row| {
        Ok(Cat {
            id: row.get(0)?,
            name: row.get(1)?,
            coat: row.get(2)?,
            weight: row.get(3)?,
        })
    })?;

    for _cat in results {
        cats.push(_cat.unwrap());
    }

    Ok(cats)
}

pub fn clear_cats() -> Result<(), rusqlite::Error> {
    let conn = Connection::open("cats.db")?;

    conn.execute("DELETE FROM cat_colors", NO_PARAMS)?;
    Ok(())
}
