#![feature(proc_macro_hygiene, decl_macro)] // opt into nightly features

#[macro_use] // imports all macros along with the crate
extern crate rocket;

pub mod data;

mod main_test;

/// Calculates and returns the probability that *any one* dice will
/// be equal to or greater than the target value
fn single_meets_probability(faces: u8, number_of_dice: u8, bonus: u8, target_value: u8) -> f64 {
    let _faces: f64 = faces.into();
    let _number_of_dice: f64 = number_of_dice.into();
    let _bonus: f64 = bonus.into();
    let _target_value: f64 = target_value.into();

    let mut fail_per_dice = ((_target_value - 1.0 - _bonus) * (100.0 / _faces)) / 100.0;

    if fail_per_dice < 0.0 {
        fail_per_dice = 0.0;
    }

    let mut result = fail_per_dice.powi(number_of_dice.into()) * 100.0;

    result = match result {
        // weird "match guard" syntax is required for some reason here
        x if x < 0.0 => 0.0,
        x if x > 100.0 => 100.0,
        _ => 100.0 - result,
    };

	(result * 100.0).round() / 100.0 // 100 to get 2 decimal places
}

/// Calculates and returns the probabilities that *any one* dice will
/// be equal to or greater than any possible value
fn single_meets_probability_enumerated(faces: u8, number_of_dice: u8, bonus: u8) -> Vec<f64> {
    let mut vec = Vec::with_capacity(faces.into());

    for i in 1..faces + 1 {
        vec.push(single_meets_probability(faces, number_of_dice, bonus, i));
    }

    vec
}

/// Calculates and returns the probability that the sum of all dice will
/// be equal to or greater than the target value
fn total_meets_probability(_faces: u8, _number_of_dice: u8, _bonus: u8, _target_value: u8) -> f64 {
    0.0
}

#[get("/d20_prob/<bonus>/<target_value>")]
fn d20_prob(bonus: u8, target_value: u8) -> String {
    single_meets_prob(20, 1, bonus, target_value)
}

#[get("/single_meets_prob/<faces>/<number_of_dice>/<bonus>/<target_value>")]
fn single_meets_prob(faces: u8, number_of_dice: u8, bonus: u8, target_value: u8) -> String {
    format!(
        "Probability: {}%",
        single_meets_probability(faces, number_of_dice, bonus, target_value)
    )
}

#[get("/single_meets_prob_enumerated/<faces>/<number_of_dice>/<bonus>")]
fn single_meets_prob_enumerated(faces: u8, number_of_dice: u8, bonus: u8) -> String {
    format!(
        "Probability: {:?}%",
        single_meets_probability_enumerated(faces, number_of_dice, bonus)
    )
}

#[get("/total_meets_prob/<faces>/<number_of_dice>/<bonus>/<target_value>")]
fn total_meets_prob(faces: u8, number_of_dice: u8, bonus: u8, target_value: u8) -> String {
    format!(
        "Probability: {}%",
        total_meets_probability(faces, number_of_dice, bonus, target_value)
    )
}

fn main() {
    if data::init().is_ok() {
        let sebastian = data::Cat{id: 0, name: "Sebastian".to_string(), coat: "Orange n' floofy!".to_string(), weight: "sleek".to_string()};
        let selene = data::Cat{id: 0, name: "Selene".to_string(), coat: "Always formal".to_string(), weight: "C H O N K".to_string()};

        match data::add_cat(&sebastian) {
            Ok(_) => println!("Added cat: {:?}", &sebastian),
            Err(e) => println!("Failed to add cat: {:?}\nError: {}", &sebastian, e)
        };
        match data::add_cat(&selene) {
            Ok(_) => println!("Added cat: {:?}", &selene),
            Err(e) => println!("Failed to add cat: {:?}\nError: {}", &selene, e)
        };

        match data::get_cats() {
            Ok(cats) => println!("Found cats! {:?}", cats),
            Err(e) => println!("Failed to herd cats :(\nError: {}", e)
        };

        match data::clear_cats() {
            Ok(_) => println!("Cleared cats table!"),
            Err(e) => println!("Failed to clear cats :(\nError: {}", e)
        }
    }

    rocket::ignite()
        .mount(
            "/",
            routes![d20_prob, single_meets_prob, single_meets_prob_enumerated],
        )
        .launch();
}
