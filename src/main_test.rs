use super::data;

#[test]
fn test_dice() {
    assert_eq!(super::single_meets_probability(20, 0, 0, 1), 0.0);
    assert_eq!(super::single_meets_probability(20, 1, 0, 1), 100.0);
    assert_eq!(super::single_meets_probability(20, 1, 0, 20), 5.0);
    assert_eq!(super::single_meets_probability(20, 2, 0, 20), 9.75);

    assert_eq!(super::single_meets_probability(20, 1, 9, 1), 100.0);
    assert_eq!(super::single_meets_probability(20, 1, 9, 20), 50.0);
    assert_eq!(super::single_meets_probability(20, 2, 9, 20), 75.0);

    assert_eq!(super::single_meets_probability(6, 1, 0, 1), 100.0);
    assert_eq!(super::single_meets_probability(6, 1, 0, 6), 16.67);
    assert_eq!(super::single_meets_probability(6, 2, 0, 6), 30.56);

    assert_eq!(super::single_meets_probability(100, 1, 0, 1), 100.0);
    assert_eq!(super::single_meets_probability(100, 1, 0, 100), 1.0);

    assert_eq!(super::single_meets_probability_enumerated(20, 1, 0),
        [100.0, 95.0, 90.0, 85.0, 80.0, 75.0, 70.0, 65.0, 60.0, 55.0,
        50.0, 45.0, 40.0, 35.0, 30.0, 25.0, 20.0, 15.0, 10.0, 5.0].to_vec());

    assert_eq!(super::single_meets_probability_enumerated(20, 2, 0),
        [100.0, 99.75, 99.0, 97.75, 96.0, 93.75, 91.0, 87.75, 84.0, 79.75,
        75.0, 69.75, 64.0, 57.75, 51.0, 43.75, 36.0, 27.75, 19.0, 9.75].to_vec());

    assert_eq!(super::single_meets_probability_enumerated(20, 2, 2),
        [100.0, 100.0, 100.0, 99.75, 99.0, 97.75, 96.0, 93.75, 91.0, 87.75,
        84.0, 79.75, 75.0, 69.75, 64.0, 57.75, 51.0, 43.75, 36.0, 27.75].to_vec());
}

#[test]
fn test_data() {
    if data::init().is_ok() {
        let sebastian = data::Cat{id: 1, name: "Sebastian".to_string(), coat: "Orange n' floofy!".to_string(), weight: "sleek".to_string()};
        let selene = data::Cat{id: 2, name: "Selene".to_string(), coat: "Always formal".to_string(), weight: "C H O N K".to_string()};

        match data::add_cat(&sebastian) {
            Ok(_) => println!("Added cat: {:?}", &sebastian),
            Err(e) => println!("Failed to add cat: {:?}\nError: {}", &sebastian, e)
        };
        match data::add_cat(&selene) {
            Ok(_) => println!("Added cat: {:?}", &selene),
            Err(e) => println!("Failed to add cat: {:?}\nError: {}", &selene, e)
        };

        match data::get_cats() {
            Ok(cats) => {
                println!("Found cats! {:?}", cats);
                assert_eq!(&cats[0], &sebastian);
                assert_eq!(&cats[1], &selene);
            },
            Err(e) => println!("Failed to herd cats :(\nError: {}", e)
        };

        match data::clear_cats() {
            Ok(_) => println!("Cleared cats table!"),
            Err(e) => println!("Failed to clear cats :(\nError: {}", e)
        }

        match data::get_cats() {
            Ok(cats) => {
                println!("Found cats! {:?}", cats);
                assert!(cats.len() == 0, "Found cats after clearing!");
            },
            Err(e) => println!("Failed to herd cats :(\nError: {}", e)
        };
    }
}
